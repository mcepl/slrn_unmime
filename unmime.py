#!/usr/bin/python
import sys,email,subprocess
progname = sys.argv[0]
sigwarning = False

def renderMessage(m):
    m_type = m.get_content_type()
    m_name = m.get_filename(None)
    if m_name:
        print "[begin attachment: %s]" % m_name
    if m_type == 'text/plain':
        print m.get_payload(decode=True)
    elif m_type == 'text/html':
        print "[%s: html rendered using lynx]" % progname
        sys.stdout.flush()
        p = subprocess.Popen(["lynx", "-stdin", "-dump"],
                             shell=False, stdin=subprocess.PIPE)
        p.stdin.write(m.get_payload(decode=False))
        p.stdin.close()
        p.wait()
    else:
        print " [unhandled content: %s %s]" % (m_type, m_name)
    if m_name:
        print "[end attachment: %s]" % m_name

def handleAlternative(m):
    parts = m.get_payload()
    for part in parts:
        if part.get_content_type() == 'text/plain':
            renderMessage(part)
            return
    for part in parts:
        if part.get_content_type() == 'text/html':
            renderMessage(part)
            return
    print "[%s: didn't find any recognizable alternatives, so dumping everything]" % progname
    handleMixed(m)

def handleSigned(m):
    for part in m.get_payload():
        part_type = part.get_content_type()
        if part_type == 'application/pgp-signature':
            pass
        else:
            if sigwarning: print "[%s: SIGNATURE NOT VERIFIED]\n" % progname
            handleMessage(part)
            if sigwarning: print "[%s: SIGNATURE NOT VERIFIED]\n" % progname

def handleMixed(m):
    for part in m.get_payload():
        handleMessage(part)

def handleMultipart(m):
    content_type = m.get_content_subtype()
    if content_type == 'alternative':
        handleAlternative(m)
    elif content_type == 'mixed':
        handleMixed(m)
    elif content_type == 'signed':
        handleSigned(m)
    else:
        print "unknown multipart content type:",content_type

def handleMessage(m):
    if m.is_multipart():
        handleMultipart(m)
    else:
        renderMessage(m)

msg = email.message_from_file(sys.stdin)

for line in msg.as_string().split('\n'):
    if not line.startswith('Content-Type:'):  print line
    if line == '': break

handleMessage(msg)
